# nome-readme

'nome is a beautifully designed app that calculates the tempo of the user's tapping in beats per minute, and can play back that tempo. It is also a standard metronome.

This app does not collect or store any information about the user or its usage. 

For questions or feedback, email luyuan.nathan[at]gmail[dot]com
